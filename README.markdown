<html>
Small and useful Shell Scripts.
=

alias_cli.sh
-
>Manage your Aliases from the command line.

copy_paste.sh
-
>Copy & paste multiple files and folders via command line accross different Shells.

dirclean.sh
-
>Remove all unwanted files in the current directory.


useful_aliases.sh
-
>Well, some useful aliases ;)


How to use the files:
=
Make the .sh file executable and link to
it from your execute path:


```$ cd /usr/bin```

```$ sudo ln -s /path/to/file.sh exec_name```

execute in shell with:


```$ exec_name```

In some .sh files change the global variables for file paths if needed.

Also make sure that you have you _$EDITOR_ environment variable set. 
To do so, add to your .bashrc file (according to your editor of choice) :

```export EDITOR="/usr/bin/emacs -nw"```
</html>
