#!/bin/bash
# ..........................................
# Manage your Aliases from the command line.
# ..........................................
# Tim Sterker, January 2012
#
# TODO:
# =====
#  + ...
#
# INFO:
# =====
# Make sure to change the variables ALIAS_DIR and ALIAS_FILE as needed.
# You can change PROMPT=0 if you dont want a prompt before adding/deleting aliases.
#

# with tailing slash
ALIAS_DIR="/media/DATA/dotfiles/"
ALIAS_FILE=".bashrc_aliases"
ALIAS_DEST=$ALIAS_DIR$ALIAS_FILE #dont touch

# Prompt before action?
PROMPT=0

function bold()
{
    BOLD="\e[1;37m" # White - Bold
    DEFAULT="\e[0m"    # Text Reset
    echo -e ${BOLD}$1${DEFAULT}
}

function prompt()
{
    read -p "${1} <RET>"
}

function mka_usage()
{
    me=$(basename $0)
    echo "$(bold "usage"):"
    echo "    ${me} <alias-name> <command>   -  $(bold "add alias")"
    echo "    ${me} $(bold -cd) <alias-name>         -  add $(bold "cd alias")"
    echo "    ${me} $(bold -d) <alias-name>          -  $(bold "delete") alias"
    echo "    ${me} $(bold -l) [<search-key>]        -  $(bold "search") in aliases"
    echo "    ${me} $(bold -e)                       -  $(bold "edit") alias file"
    exit 1
}

function touch_alias_dest(){
    if [ ! -d "$ALIAS_DIR" ]; then
        echo "alias directory $ALIAS_DIR does not exist."
        echo "Make sure to change your ALIAS_DIR or create the directory."
        exit 1
    fi
    touch $ALIAS_DEST
    cp $ALIAS_DEST $ALIAS_DEST.bak     # just to be save
}

function mka_cdalias(){
    touch_alias_dest
    path=$(pwd)
    name=$1
    alias="alias $@=\"cd ${path}\""

    # delete if exists
    [[ $(cat $ALIAS_DEST | grep "alias ${name}=") ]] && prompt "$(bold "Alias already exists! Override?")" && mka_delete $name

    [[ $PROMPT == 1 ]] && prompt "$(bold "add cd-alias") to: ${path}"
    echo "appended to: "$ALIAS_DEST
    echo "$alias" >> $ALIAS_DEST
    return 0
}

function mka_append(){
    touch_alias_dest
    name=$1
    command=${@:2}
    local alias="alias ${name}=\"${command}\""

    # delete if exists
    [[ $(cat $ALIAS_DEST | grep "alias ${name}=") ]] && prompt "$(bold "Alias already exists! Override?")" && mka_delete $name

    [[ $PROMPT == 1 ]] && prompt "$(bold "add alias for:") ${command}"
    echo "appended to: "$ALIAS_DEST
    echo $alias >> $ALIAS_DEST
    return 0
}

function mka_delete(){
    touch_alias_dest
    local alias="alias $1"

    [[ $PROMPT == 1 ]] && prompt "$(bold "Delete alias?")"
    echo "deleted from: "$ALIAS_DEST
    grep -v "$alias" "$ALIAS_DEST.bak" > "$ALIAS_DEST.tmp" # -v excludes $alias
    mv $ALIAS_DEST.tmp $ALIAS_DEST
    return 0
}

function mka_search()
{
    if [[ $1 == "" ]]; then
        cat $ALIAS_DEST
    else
        echo "Results for:" $(bold $1)
        echo "=========================="
        cat $ALIAS_DEST | grep $1 --color
        [[ $? == 1 ]] && echo "No results."
    fi

}

if [[ $# == 0 ]]; then # wrong parameters, exit
    mka_usage
fi
if [[ $1 == "-d" ]]; then # delete alias
    mka_delete $2
elif [[ $1 == "-cd" ]]; then # add cd alias
    mka_cdalias $2
elif [[ $1 == "-l" ]]; then # search in aliases
    mka_search $2
elif [[ $1 == "-e" ]]; then # edit file
    $EDITOR $ALIAS_DEST
else # add alias
    mka_append $@
fi
source $ALIAS_DEST  # maybe doesn't work
