#!/bin/bash
# .............................................................................
# Copy paste (multiple) files/folders via Command Line accross different Shells
# .............................................................................
# Tim Sterker, January 2012
#
# TODO:
# =====
#   + ...
#

TMP_FILE="$HOME/.tmp_cnp"

# styles for stdout
BOLD="\e[1;37m" # White - Bold
DEFAULT="\e[0m"    # Text Reset

function cnp_die(){
    echo $1 && exit 1
}

function bold()
{
    echo -e ${BOLD}$1${DEFAULT}
}

function cnp_usage(){
    me=$(basename $0)
    echo "$(bold usage):"
    echo "    ${me} [<file>|<folder>]$(bold ...)  - copy space separated list of files"
    echo "    ${me} $(bold paste)                 - paste all previously copied files and delete temporary copy-file"
    echo "    ${me} $(bold "paste -k")              - paste but keep temporary copy-file"
    echo "    ${me} $(bold "paste -c")              - cut & paste the copied files"
    echo "    ${me} $(bold show)                  - show all files to be copied"
    echo "    ${me} $(bold edit)                  - open temporary copy-file to edit by hand"
    echo "    ${me} $(bold clear)                 - delete temporary copy-file"
    exit 1
}

function cnp_clear(){
    rm $TMP_FILE 2> /dev/null
    return 0
}

# append to copy list until first paste, then the list is cleared
function cnp_copy(){
    touch $TMP_FILE
    cur_dir=$(pwd)"/"
    for filename in $@; do
        local filepath=$cur_dir$filename
        if [[ -f $filepath ]]; then
            echo "copy FILE --> "$cur_dir$filename
            echo $filepath >> $TMP_FILE
        elif [[ -d $filepath ]]; then
            echo "copy DIR --> "$filepath
            echo $filepath >> $TMP_FILE # add recursive option
        fi
    done
}

function cnp_paste(){
    txt=$(cat $TMP_FILE)

    for word in $txt; do
        if [[ -f $word ]]; then
            echo "paste FILE --> $word"
            [[ $1 == "-c" ]] && mv $word . || cp $word .
        elif [[ -d $word ]]; then
            echo "paste DIR --> $word"
            [[ $1 == "-c" ]] && mv -r $word . || cp -r $word .
        else
            echo "File ${word} doesn't exist anymore."
        fi

    done
    [[ $1 == "-k" ]] && cnp_die "Keeping temporary copy-file for further pastes."
    echo "Temporary copy-file deleted"
    cnp_clear
}

function cnp_show(){
    [[ -f $TMP_FILE ]] ||  cnp_die "There were no files copied yet."
    echo "Files to copy:"
    for file in $(cat $TMP_FILE); do
        echo $file
    done
}

if [[ ${#@} < 1 ]]; then # wrong usage
    cnp_usage $0
elif [[ $1 == "paste" ]]; then
    cnp_paste $2 # pass optional 2nd parameter
elif [[ $1 == "show" ]]; then
    cnp_show
elif [[ $1 == "clear" ]]; then
    cnp_clear
elif [[ $1 == "edit" ]]; then
    $EDITOR $TMP_FILE
else
    cnp_copy ${@:1}
fi
