#!/bin/bash
# ..........................................
# Remove all unwanted files in the current directory
# ..........................................
# Tim Sterker, January 2012
#
# TODO:
# =====
#   + mark the matched extensions (like in grep --color), but how?
#

# filetypes to remove
TO_CLEAN="*.bak *.tmp *.pyc *.o *.*~ *~"

function bold()
{
    BOLD="\e[1;37m" # White - Bold
    DEFAULT="\e[0m"    # Text Reset
    echo -e ${BOLD}$1${DEFAULT}
}

function msg_on_error()
{
    [[ $? != 0 ]] && echo $1 && return 1
    return 0
}

function dc_clean()
{
    echo $(bold "Cleaning files:")
    printed=""
    for file in $TO_CLEAN; do
	[[ -f $file ]] && echo "    "$file && printed=$printed$$file
    done
    [[ $printed == "" ]] && echo $(bold "No files to clean up.")
    read -p "<RET>"

    for file in $TO_CLEAN; do
	rm ./$file 2> /dev/null
#	msg_on_error "no $file files deleted"
    done
}

dc_clean
