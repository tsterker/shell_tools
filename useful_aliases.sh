#! /bin/bash

# note: use alias_cli.sh for alias management from command line

# convenience
alias lgrep="ls -l | grep"  # search in current directory
alias ..="cd .."            # cd to parent directory
alias cdd="cd $OLDPWD"      # cd to where you were before
alias x="exit"              # exit shell

# Set up Emacs as an edit server:
# Emacs processes share buffers, command history and other kinds of information with any existing emacs(-client) processes.
alias ed="emacs --daemon" # start emacs server, do once after bootup
alias ec="emacsclient -t" # connect to existing emacs process/daemon (-t in terminal)

# make rm undoable by moving to trash
#alias rm='mv -t ~/.local/share/Trash/files/'

